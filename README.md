**Installing the MVC Music Store Project**

To install the MVCMusicStore project in your computer, please follow these steps:

1. Clone the repository in a folder of your local computer.
2. Open Visual Studio.
3. Click the **Open a project or solution** button.
4. Navigate to the folder in your computer where your cloned code resides
5. Select the **MvcMusicStore.sln** file and then click the **Open** button.
6. Visual Studio will load the project and it will be ready to be used.

---

**Software License**

MIT License
Copyright (c) 2021 Byoughak Yoo, Jiyoung Jung, Jisu Ok, Renzo Moran, Sean Yang.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

---

**Note**

We chose to use MIT License as our team decided to publish this project as open source so it will be open for everyone to see and use the code as desired.
Having said this, we also wanted to make sure that our source code is provided as it is, with no warranty at all.