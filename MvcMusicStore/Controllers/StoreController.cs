﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MvcMusicStore.Models;

namespace MvcMusicStore.Controllers
{
    public class StoreController : Controller
    {
        public IActionResult Index()
        {
            //return "Hello from store Index";
            //return RedirectToAction("RequestStuff", "Store", new { contact = "Email" });

            Response.Cookies.Append("ArtistName", "New Artist on the block", new CookieOptions { Expires = DateTime.Today.AddHours(2)});

            HttpContext.Session.SetInt32("ArtistID", 100);
            
            return RedirectToAction("RequestStuff", "Store", new { contact = "Email", userName="John"});
        }

        public string browse(string genre)
        {
            //Request.Query["genre"].FirstOrDefault();
            return "We got " + genre;
        }

        public string Details(Int32 id)
        {
            return "We got " + id;
        }

        public IActionResult Sample()
        {
            ViewBag.message = "this is the ViewBag property 'message'";
            return View();
        }

        public IActionResult List()
        {
            List<Album> albums = new List<Album>();
            for (int i = 0; i < 10; i++)
            {
                albums.Add(new Album { Title = "Generic Album " + i });
            }
            //ViewBag.Albums = albums; // create new entry in ViewData
            return View(albums);           // render ~/Views/Store/List.cshtml

        }
        public IActionResult Guest()
        {
            return View();
        }

        public IActionResult testShoppingCart()
        {

            ShoppingCartViewModel obj = new ShoppingCartViewModel();
            obj.album.Title = "Album1";

            obj.artist.Name = "Artist 1";
            ViewData["ShoppingCart"] = obj;

            return View(obj);
        }

        public IActionResult RequestStuff(string user)
        {
            
            //if (user != null) HttpContext.Session.SetString("user", user);
            //var userName = Request.Query["user"];
            var userName = "TestUser";
            ViewData["Message"] = $"Your application description page, '{userName}'";
            Response.Cookies.Append("userName", userName);
            List<Generic2String> headers = new List<Generic2String>();
            foreach (var item in Request.Headers.Keys)
            {
                headers.Add(new Generic2String($"Request.Headers['{item}']", Request.Headers[item]));
            }
            headers.Add(new Generic2String("Request.Headers['Referer']", Request.Headers["Referer"]));
            headers.Add(new Generic2String("Request.Host", Request.Host.ToString()));
            foreach (var item in Request.Cookies.Keys)
            {
                headers.Add(new Generic2String($"Request.Cookies['{item}']", Request.Cookies[item]));
            }
            headers.Add(new Generic2String("Request.HttpContext.Connection.LocalIpAddress (user IP)",
                        Request.HttpContext.Connection.LocalIpAddress.ToString()));
            headers.Add(new Generic2String("Request.HttpContext.Connection.LocalPort (user TCP port)",
                        Request.HttpContext.Connection.LocalPort.ToString()));
            headers.Add(new Generic2String("Request.HttpContext.Connection.RemoteIpAddress (server IP)",
                        Request.HttpContext.Connection.RemoteIpAddress.ToString()));
            headers.Add(new Generic2String("Request.HttpContext.Connection.RemotePort (server TCP port)",
                        Request.HttpContext.Connection.RemotePort.ToString()));
            //headers.Add(new Generic2String("Request.HttpContext.Session.Id", Request.HttpContext.Session.Id.ToString()));
            headers.Add(new Generic2String("Request.IsHttps", Request.IsHttps.ToString()));
            headers.Add(new Generic2String("Request.Method", Request.Method.ToString()));
            headers.Add(new Generic2String("Request.Path", Request.Path.ToString()));
            headers.Add(new Generic2String("Request.Protocol", Request.Protocol.ToString()));
            headers.Add(new Generic2String("Request.QueryString", Request.QueryString.ToString()));
            headers.Add(new Generic2String("Request.Query", Request.Query.ToString()));
            foreach (var item in Request.Query)
            {
                headers.Add(new Generic2String($"Request.Query['{item.Key}']", item.Value));
            }
            string ArtistName = Request.Cookies["ArtistName"].ToString();
            headers.Add(new Generic2String("Artist Name", ArtistName));

            //int ArtistID = Convert.ToInt32( HttpContext.Session.GetInt32("ArtistID"));
            //headers.Add(new Generic2String("ArtistId", ArtistID.ToString()));

            TempData["message"] = "In RequestStuff. All good";
            return View(headers);

        }
    }
}