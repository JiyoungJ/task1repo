﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMusicStore.Models
{
    [ModelMetadataTypeAttribute(typeof(OrderMetadata))]
    public partial class Order
    { }

    public class OrderMetadata
    {
        public int OrderId { get; set; }

        [Remote("OrderDateNotFuture", "Checkout")]
        public DateTime OrderDate { get; set; }
        public string UserName { get; set; }

        [StringLength(20, MinimumLength = 3, ErrorMessage = "")]
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [StringLength(20)]
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string ProvinceCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }

        [RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Please enter a valid phone number")]
        public string Phone { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public double Total { get; set; }

    }
}
