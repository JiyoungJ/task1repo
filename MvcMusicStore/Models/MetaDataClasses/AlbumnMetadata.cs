﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMusicStore.Models
{
    [ModelMetadataTypeAttribute(typeof(AlbumnMetadata))]
    public partial class Album
    { }

    public class AlbumnMetadata
    {

        public int AlbumId { get; set; }
        public int GenreId { get; set; }
        public int ArtistId { get; set; }

        [Required]
        public string Title { get; set; }
        public double Price { get; set; }
        public string AlbumArtUrl { get; set; }
    }
}
