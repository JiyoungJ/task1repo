﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MvcMusicStore.Models;

namespace MvcMusicStore.Controllers
{
    public class CheckoutController : Controller
    {
        private readonly MvcMusicStoreContext _context;

        public CheckoutController(MvcMusicStoreContext context)
        {
            _context = context;
        }

        public IActionResult AddressAndPayment()
        {
            Order order = new Models.Order();
            order.OrderDate = DateTime.Now;

            ViewBag.CountryCode = new SelectList(_context.Country.OrderBy(a => a.Name), "CountryCode", "Name");
            ViewBag.ProvinceCode = new SelectList(_context.Province.OrderBy(a => a.Name), "ProvinceCode", "Name");

            return View(order);
        }

        public JsonResult OrderDateNotFuture(DateTime orderDate)
        {
            Boolean isTodayOrBefore = orderDate <= DateTime.Now;

            if (isTodayOrBefore)
            {
                return Json(isTodayOrBefore);
            }
            else
            {
                return Json("Order date cannot be in the future.");
            }
            
        }
    }
}
