﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMusicStore.Models
{
    public class ShoppingCartViewModel
    {
        public Album album { get; set; }

        public Artist artist { get; set; }

    }
}
